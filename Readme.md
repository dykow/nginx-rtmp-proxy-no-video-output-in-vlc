To run

```
docker run --name nginx-rtmp \
    -p 1935:1935 \
    -p 8080:8080 \
    -v $PWD/nginx.conf:/etc/nginx/nginx.conf:z \
    -d nginx-rtmp:local
```
